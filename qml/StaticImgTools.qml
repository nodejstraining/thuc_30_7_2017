import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
Item {
    property bool staticToolVisible: false
    property int angleSticker: 0
    Column{
        visible: staticToolVisible

        x: 300
        y: 0
        Row{
            Column{
                Text{text: "WaitTime"}
                SpinBox{
                    id: staticWaitSpinbox
                    maximumValue: 1000000000
                    minimumValue: 0
                    value: (statictModels.get(index).StaticImgWaitTime === 0) ? 1000 : statictModels.get(index).StaticImgWaitTime
                }
            }
            Column{
                Text{text: "AnimateTime"}
                SpinBox{
                    id: staticAnimateSpinbox
                    maximumValue: 1000000000
                    minimumValue: 0
                    value: 3000
                }
            }
            Column{
                Image {
                    id: name
                    width: 40
                    height: 40
                    source: "../images/Rotate_right.png"
                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            angleSticker += 5
                        }
                    }
                }
            }
            Column{
                Image {
                    id: rotateLeft
                    width: 40
                    height: 40
                    source: "../images/Rotate_left.png"
                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            angleSticker -= 5
                        }
                    }
                }
            }
            Image{
                id: stickerAfter
                width: 40
                height: 40
                source: "../images/sticker.png"
                MouseArea{
                    id: stickerSelectMulti
                    anchors.fill: parent
                    onClicked: {
                        stickerMovingDialog.open()
                        stickerMovingDialog.selectMultiple
                    }
                }
            }
            Image{
                id: staticStickerAfter
                width: 40
                height: 40
                source: "../images/static_sticker.png"
                MouseArea{
                    id: staticSelectMutil
                    anchors.fill: parent
                    onClicked: {
                        stickerStaticDialog.open()
                        stickerStaticDialog.selectMultiple
                    }
                }
            }

            Button{
                id: btnUpdate
                text: "Update"
                onClicked: {
                    dialogUp8.open()
                }
            }
        }
    }
    //File dialog cho viec chon file sticker di chuyen
    FileDialog{
        id: stickerMovingDialog
        nameFilters: ["*.png", "*.jpg", "All file(*)"]
        folder: shortcuts.music+"/bibi_data/characters/thumbnail"
        selectMultiple: stickerSelectMulti.acceptedButtons
        onAccepted: {
            var arrUrls = [], listUrls = []
            arrUrls = stickerMovingDialog.fileUrls
            for(var i = 0; i < arrUrls.length; i++){
                listUrls.push({src: arrUrls[i]})
            }
            var coorImg = [{
                    x: statictModels.get(index).CoorStaticImg.get(0).x,
                    y: statictModels.get(index).CoorStaticImg.get(0).y
                 }]
            var myWaitTime = statictModels.get(index).StaticImgWaitTime+statictModels.get(index).StaticImgAnimateTime
            stickerModels.append({
                                     SrcSticker: listUrls,
                                     CoorsImg: coorImg,
                                     CoorsMove: coorImg,
                                     waitTime: myWaitTime,
                                     animateTime: 0,
                                     hideImg: false,
                                     visibleTools: false,
                                     isAfter: true,
                                     angleSticker: 0
                                 })
            statictModels.set(index, {StaticImgHide: true})
        }
    }
    //FileDialog cho viec chon sticker tinh
    FileDialog{
        id: stickerStaticDialog
        nameFilters: ["*.png", "*.jpg", "All file(*)"]
        folder: shortcuts.music+"/bibi_data/characters/thumbnail"
        selectMultiple: staticSelectMutil.acceptedButtons
        onAccepted: {
            var arrUrls = [], listUrls = []
            arrUrls = stickerStaticDialog.fileUrls
            for(var i = 0; i < arrUrls.length; i++){
                listUrls.push({src: arrUrls[i]})
            }
            var coorImg = [{
                    x: statictModels.get(index).CoorStaticImg.get(0).x,
                    y: statictModels.get(index).CoorStaticImg.get(0).y
                 }]
            var myWaitTime = statictModels.get(index).StaticImgWaitTime+statictModels.get(index).StaticImgAnimateTime

            statictModels.append({
                                     SrcStatic: listUrls,
                                     CoorStaticImg: CoorsImg,
                                     StaticImgWaitTime: myWaitTime,
                                     StaticImgAnimateTime: 3000,
                                     StaticImgHide: false,
                                     StaticImgisAfter: true,
                                     StaticImgAngleSticker: 0,
                                     StaticVisibleTool: false
                                 })
            statictModels.set(index, {StaticImgHide: true})
        }
    }

    Dialog{
        id: dialogUp8
        title: "update"
        standardButtons: StandardButton.Save | StandardButton.Cancel
        onAccepted: {
//            console.log(staticsticker.xStaticImg)
            var CoorStaticImg = [{x: staticsticker.xStaticImg, y: staticsticker.yStaticImg}]
            statictModels.set(index, {
                CoorStaticImg: CoorStaticImg,
                StaticImgWaitTime: staticWaitSpinbox.value,
                StaticImgAnimateTime: staticAnimateSpinbox.value,
                StaticImgAngleSticker: angleSticker
            })
        }
    }
}
