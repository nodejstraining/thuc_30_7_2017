import QtQuick 2.5
import QtQuick.Dialogs 1.2

Item {
    property int name: 0
    Rectangle{
        ListModel{
            id: statictModels
            ListElement{
                SrcStatic: [ListElement{src: ""}]
                CoorStaticImg: [ListElement{x: 0; y: 0}]
                StaticImgWaitTime: 0
                StaticImgAnimateTime: 0
                StaticImgHide: false
                StaticImgisAfter: false
                StaticImgAngleSticker: 0
                StaticVisibleTool: false
            }
        }
        FileDialog{
            id: dialogStatic
            nameFilters: ["*.png", "*.jpg", "All file(*)"]
            folder: shortcuts.music+"/bibi_data/characters/thumbnail"
            selectMultiple: btnOpenFile.__action
            onAccepted: {
                var arrUrls = [], listUrls = []
                arrUrls = dialogStatic.fileUrls
                for(var i = 0; i < arrUrls.length; i++){
                    listUrls.push({src: arrUrls[i]})
                }
                var coors = [{x: 30, y: 40}]
                statictModels.append({
                    SrcStatic: listUrls,
                    CoorStaticImg: coors
                })
            }
        }
    }
}
