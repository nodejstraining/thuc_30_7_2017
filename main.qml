import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import myTimer 1.0
import "CreateImage.js" as CreImage
import "js/StaticImgCom.js" as CreStaticImg
import "qml"

Window {
    visible: true
    width: Screen.width -1
    height: Screen.height -1
    id: roots
    property var background: "images/backgroundImage.png"
    property int xDefault: roots.width/2
    property int yDefault: roots.height/2
    //dung de dinh nghia start voi stop
    property bool isStart: false

    property bool stateFix: false
    //Tao listmodel de luu tru hinh anh va toa do cua hinh anh do
    Rectangle{
        //Models cua sticker co the di chuyen
        ListModel{
            id: stickerModels
            ListElement{
                SrcSticker: [ListElement{src: ""}]
                CoorsMove: [ListElement{x: 0; y: 0}]
                CoorsImg: [ListElement{x: 0; y: 0}]
                waitTime: 0
                animateTime: 0
                hideImg: false
                visibleTools: false
                isAfter: false
                angleSticker: 0
            }
        }
        //Models cua sticker khong the di chuyen
        ListModel{
            id: statictModels
            ListElement{
                SrcStatic: [ListElement{src: ""}]
                CoorStaticImg: [ListElement{x: 0; y: 0}]
                StaticImgWaitTime: 0
                StaticImgAnimateTime: 0
                StaticImgHide: false
                StaticImgisAfter: false
                StaticImgAngleSticker: 0
                StaticVisibleTool: false
            }
        }
        //Mo file de chon back ground
        FileDialog{
            id: bgDialog
            nameFilters: ["*.png", "*.jpg", "All file(*)"]
            folder: shortcuts.music+"/bibi_data/characters/thumbnail"
            onAccepted: {
                background = bgDialog.fileUrl
            }
        }
        //Mo file de chon Sticker
        FileDialog{
            id: stickerDialog
            nameFilters: ["*.png", "*.jpg", "All file(*)"]
            folder: shortcuts.music+"/bibi_data/characters/thumbnail"
            selectMultiple: clickStick.acceptedButtons
            onAccepted: {
                isChooseSticker()
            }
        }

        //Background image
        Image {
            anchors.top: toolStart.bottom
            id: backgroundImage
            source: background
            width: roots.width
            height: roots.height-bgIcon.height
        }
        Row{
            id: toolStart
            //Icon background
            Image {
                id: bgIcon
                source: "images/bgicon.png"
                width: Screen.width/20
                height: Screen.height/12
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        bgDialog.open()
                    }
                }
            }
            //Icon Sticker
            Image {
                id: stickerIcon
                source: "images/sticker.png"
                width: bgIcon.width
                height: bgIcon.height

                MouseArea{
                    id: clickStick
                    anchors.fill: parent
                    onClicked: {
                        stickerDialog.open()
                        stickerDialog.selectMultiple
                    }
                }
            }
            //icon static sticker
            Image {
                id: staticStickerIcon
                width: bgIcon.width
                height: bgIcon.height
                source: "images/static_sticker.png"
                MouseArea{
                    id: click_static_Sticker
                    anchors.fill: parent
                    onClicked: {
                        dialogStatic.open()
                        dialogStatic.selectMultiple
                    }
                }
            }
            //Icon start and stop
            Image {
                id: startStop
                source: !isStart ? "images/start1.png" : "images/stop.png"
                width: bgIcon.width
                height: bgIcon.height
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        isStart = !isStart
                        if(isStart){
                            viewStaticStickerSetting.visible = false
                            listViewSticker.visible = false
                            page.visible = true
                            page.load()
                        }else if(!isStart){
                            listViewSticker.visible = true
                            viewStaticStickerSetting.visible = true
                            for(var i = 0; i < page.children.length; i++){
                                page.children[i].stateFix = true
                            }
                        }

                        if(stickerModels.count == 1 && statictModels.count ==1){
                            message.visible = true
                        }else message.visible = false

                        if((stickerModels.count == 1 && !isStart)||(statictModels.count == 1 && !isStart))
                            message.visible = false
                    }
                }
            }
        }
        Text {
            visible: false
            id: message
            x: roots.width/2
            y: roots.height/2
            text: qsTr("Please choose Image")
            color: "red"
        }
        //Listview hien thi danh sach cac sticker minh chon
        ListView{
            id: listViewSticker
            model: stickerModels
            delegate: Rectangle{
                Sticker{
                    id: mySticker
                    angleSticker: tools.angleSticker
                    xSticker: ((CoorsImg.get(0).x === 0) && (CoorsImg.get(0).y ===0)) ? xDefault : CoorsImg.get(0).x
                    ySticker: ((CoorsImg.get(0).x === 0) && (CoorsImg.get(0).y ===0)) ? yDefault : CoorsImg.get(0).y
                }
                Tools{
                    id: tools
                    toolVisible: visibleTools
                }
            }
        }
        //------------------------- lua chon static sticker ----------------------------------------
        FileDialog{
            id: dialogStatic
            nameFilters: ["*.png", "*.jpg", "All file(*)"]
            folder: shortcuts.music+"/bibi_data/characters/thumbnail"
            selectMultiple: click_static_Sticker.acceptedButtons
            onAccepted: {
                var arrUrls = [], listUrls = []
                arrUrls = dialogStatic.fileUrls
                for(var i = 0; i < arrUrls.length; i++){
                    listUrls.push({src: arrUrls[i]})
                }
                var coorsStaticImg = [{x: xDefault, y: yDefault}]
                statictModels.append({
                    SrcStatic: listUrls,
                    CoorStaticImg: coorsStaticImg,
                    StaticImgWaitTime: 0,
                    StaticImgAnimateTime: 0,
                    StaticImgHide: false,
                    StaticImgisAfter: false,
                    StaticImgAngleSticker: 0,
                    StaticVisibleTool: false
                })
            }
        }
        //Chuyen sang phan static sticker
        ListView{
            id: viewStaticStickerSetting
            model: statictModels
            delegate: Rectangle{
                StaticSticker{
                    id: staticsticker
                    xStaticImg: (
                    (CoorStaticImg.get(0).x === 0) && (CoorStaticImg.get(0).y === 0)
                     )?xDefault : CoorStaticImg.get(0).x
                    yStaticImg: (
                    (CoorStaticImg.get(0).x === 0) && (CoorStaticImg.get(0).y === 0)
                     )?yDefault : CoorStaticImg.get(0).y
                    staticImgAngleSticker: staticImgTools.angleSticker
                }
                StaticImgTools{
                    id: staticImgTools
                    staticToolVisible: StaticVisibleTool
                }
            }
        }

        //----------------------------------------------------------------------------------------------------
        //View display program
        Rectangle{
            id: page
            //du lieu cua sticker chuyen dong
            property var data : []
            property var coorsImg : []
            property var coorsMove : []
            property var waitTime : []
            property var animateTime : []
            property var hideImg: []
            property var isAfter: []
            property var angleSticker: []
            //du lieu cua sticker tinh
            property var static_data: []
            property var static_coorImg: []
            property var static_waitTime: []
            property var static_animateTime: []
            property var static_angle_sticker: []
            property var static_isAfter: []
            property var static_hideImg: []
            function load(){
                //Load va thuc hien chay chuong trinh chuyen dong
                for (var i = 0; i < stickerModels.count; i++){
                    data[i] = []
                    for(var j = 0; j < stickerModels.get(i).SrcSticker.count; j++){
                        data[i].push(stickerModels.get(i).SrcSticker.get(j).src)
                    }
                    coorsImg[i] = [{x: stickerModels.get(i).CoorsImg.get(0).x, y: stickerModels.get(i).CoorsImg.get(0).y}]
                    coorsMove[i] = [{x: stickerModels.get(i).CoorsMove.get(0).x, y: stickerModels.get(i).CoorsMove.get(0).y}]
                    waitTime[i] = stickerModels.get(i).waitTime
                    animateTime[i] = stickerModels.get(i).animateTime
                    hideImg[i] = stickerModels.get(i).hideImg
                    isAfter[i] = stickerModels.get(i).isAfter
                    angleSticker[i] = stickerModels.get(i).angleSticker
                }

                for(var i = 0; i < data.length; i++){
                    CreImage.createImage(
                                coorsImg[i],
                                waitTime[i],
                                data[i],
                                coorsMove[i],
                                animateTime[i],
                                hideImg[i],
                                isAfter[i],
                                angleSticker[i]
                    )
                }
                //---------------------------------------------------------------------------------
                //Load va thuc hien viec chay chuyen dong tai cho
                for(var ii = 0; ii < statictModels.count; ii++){
                    static_data[ii] = []
                    for(var jj = 0; jj < statictModels.get(ii).SrcStatic.count ; jj++){
                        static_data[ii].push(statictModels.get(ii).SrcStatic.get(jj).src)
                    }
                    static_coorImg[ii] = [{
                                              x: statictModels.get(ii).CoorStaticImg.get(0).x,
                                              y: statictModels.get(ii).CoorStaticImg.get(0).y
                                          }]
                    static_waitTime[ii] = statictModels.get(ii).StaticImgWaitTime
                    static_animateTime[ii] = statictModels.get(ii).StaticImgAnimateTime
                    static_angle_sticker[ii] = statictModels.get(ii).StaticImgAngleSticker
                    static_isAfter[ii] = statictModels.get(ii).StaticImgisAfter
                    static_hideImg[ii] = statictModels.get(ii).StaticImgHide
                }
                for(var iii = 0; iii< static_data.length; iii++){
                    CreStaticImg.createImage(
                                static_data[iii],
                                static_coorImg[iii],
                                static_waitTime[iii],
                                static_animateTime[iii],
                                static_angle_sticker[iii],
                                static_isAfter[iii],
                                static_hideImg[iii]
                                )
                }
            }
        }

        //------------------------------------------------------------------------------------------------------------------

    }
    //khi chon xong cac sticker
    function isChooseSticker(){
        var arrUrls = [], listUrls = []
        arrUrls = stickerDialog.fileUrls
        for(var i = 0; i < arrUrls.length; i++){
            listUrls.push({src: arrUrls[i]})
        }
        var Coors = [{x: xDefault, y: yDefault}]
        stickerModels.append({
            SrcSticker: listUrls,
            CoorsImg: Coors,
            CoorsMove: Coors,
            waitTime: 0,
            animateTime: 0,
            hideImg: false,
            visibleTools: false,
            isAfter: false,
            angleSticker: 0
         })
        message.visible = false
    }
}
