var component
function createImage(listSource, coorImg, waitTime, animateTime, staticImgAngleSticker, isAfter, hideImg){
    component = Qt.createComponent("../qml/DynamicStaticImgComp.qml")
    if(component.status === Component.Ready || component.status === Component.Error)
        finishCreation(listSource, coorImg, waitTime, animateTime, staticImgAngleSticker, isAfter, hideImg)
    else
        component.statusChanged.connect(finishCreation)
}

function finishCreation(listSource, coorImg, waitTime, animateTime, staticImgAngleSticker, isAfter, hideImg){
    if(component.status === Component.Ready){
        var image = component.createObject(page,
        {
          "listSrc": listSource,
          "coorImg": coorImg,
          "waitTime": waitTime,
          "animateTime": animateTime,
          "staticImgAngleSticker": staticImgAngleSticker,
          "isAfter": isAfter,
          "hideImg": hideImg
        })
        if(image === null)
            console.log("error creating image")
    }else if(component.status === Component.Error)
        console.log("Error to load component: ", component.errorString())
}
